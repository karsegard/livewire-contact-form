<div class="contents">
    @if (session('status'))
        <form>
            <p class="success">
                <span class="success"></span>
                {{ session('status') }}
            </p>
        </form>
    @else
        <form class="flex flex-col gap-3.75 w-full" wire:submit.prevent="send">
            @csrf
            <div class="flex flex-col md:flex-row w-full gap-3.75">

                <input wire:model="form.lastname"
                    class="typo-md w-full md:w-1/2 px-1 placeholder:text-gray
            border-2 border-solid border-black dark:border-secondary focus:outline-none focus:ring ring-gray-t focus:border-gray-t"
                    type="text" placeholder="Nom*" name="lastname" value="{{ old('lastname') }}" required />
                @error('form.lastname')
                    <span class="typo-sm">
                        <strong class="text-red-700">{{ __($message) }}</strong>
                    </span>
                @enderror
                <input wire:model="form.firstname"
                    class="typo-md w-full md:w-1/2 px-1 placeholder:text-gray
            border-2 border-solid border-black dark:border-secondary focus:outline-none focus:ring ring-gray-t focus:border-gray-t"
                    type="text" placeholder="Prénom*" name="firstname" value="{{ old('form.firstname') }}"
                    required />
                @error('form.firstname')
                    <span class="typo-sm">
                        <strong class="text-red-700">{{ __($message) }}</strong>
                    </span>
                @enderror

            </div>


            <input wire:model="form.email"
                class="typo-md w-full px-1 placeholder:text-gray
        border-2 border-solid border-black dark:border-secondary focus:outline-none focus:ring ring-gray-t focus:border-gray-t"
                type="email" placeholder="Votre Email*" name="email" required value="{{ old('form.email') }}" />
            @error('form.email')
                <span class="typo-sm">
                    <strong class="text-red-700">{{ __($message) }}</strong>
                </span>
            @enderror




            <textarea wire:model="form.message"
                class="typo-md w-full px-1 placeholder:text-gray
        border-2 border-solid border-black dark:border-secondary focus:outline-none focus:ring ring-gray-t focus:border-gray-t"
                name="message" rows="6" cols="80" placeholder="Votre message*">{{ old('form.message') }}</textarea>
            @error('form.message')
                <span class="typo-sm">
                    <strong class="text-red-700">{{ __($message) }}</strong>
                </span>
            @enderror

            <div>{!! $captcha !!}</div>
            <input wire:model="form.captcha"
                class="typo-md w-full px-1 placeholder:text-gray
        border-2 border-solid border-black dark:border-secondary focus:outline-none focus:ring ring-gray-t focus:border-gray-t"
                type="text" placeholder="Captcha*" name="captcha" value="" />
            @error('form.captcha')
                <span class="typo-sm">
                    <strong class="text-red-700">{{ __($message) }}</strong>
                </span>
            @enderror

            <button class="self-end" type="submit">Envoyer</button>
        </form>
    @endif

</div>
