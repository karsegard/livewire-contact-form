<?php
namespace KDA\Livewire\ContactForm;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasViews;
use Livewire\Livewire;

//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use HasViews;
    protected $packageName ='livewire-contact-form';
    protected $viewNamespace = 'livewire-contact-form';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    /*public function register()
    {
        parent::register();
    }*/
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
        Livewire::component(ContactForm::getName(),ContactForm::class);
    }
}
