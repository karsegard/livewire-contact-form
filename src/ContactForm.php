<?php

namespace  KDA\Livewire\ContactForm;

use App\Mail\SiteContact as Notification;

use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Validator;
use Livewire\Component;
use Mews\Captcha\Facades\Captcha;
use DanHarrin\LivewireRateLimiting\WithRateLimiting;
use Illuminate\Validation\ValidationException;
use DanHarrin\LivewireRateLimiting\Exceptions\TooManyRequestsException;
abstract class ContactForm extends Component
{
    use WithRateLimiting;
    public $form;
    public $captcha;
    protected $rules = [
        'form.firstname' => 'required|string',
        'form.lastname' => 'required|string',
        'form.email' => 'required|email',
        'form.message' => 'required|string',
        'form.captcha' => 'required|captcha'
    ];
    protected $messages = [
        'form.firstname.required' => 'Le prénom est obligatoire',
        'form.lastname.required' => 'Le nom est obligatoire',
        'form.email.required' => 'Le champs Email est obligatoire',
        'form.message.required' => 'Le message est obligatoire',
        'form.email.email' => 'L\'Adresse email n\'est pas valide',
        'form.captcha.captcha' => 'Le captcha n\'est pas valide',
    ];
    protected $throttle_message = "Attendez %d secondes pour rééssayer";
    protected $success_message = 'Votre message a été envoyé';
    protected bool $shouldTranslate=true;
    public function render()
    {
        return view('livewire-contact-form::contact-form');
    }
    public function mount(){
        //$captcha = Captcha::create('flat');
        $this->captcha = captcha_img('flat');
       
    }
    public function formSent(){
        request()->session()->flash('status', __($this->success_message));
    }
    abstract public function notify();
    public function send()
    {
        try {
            $this->rateLimit(5);
        } catch (TooManyRequestsException $exception) {
            $this->captcha = captcha_img('flat');
            $this->form['captcha']="";
            throw ValidationException::withMessages([
                'form.captcha' => sprintf(__($this->throttle_message),$exception->secondsUntilAvailable),
            ]);
        }
        
        $this->withValidator(function (Validator $validator) {
          
            $validator->after(function ($validator) {
                $this->captcha = captcha_img('flat');
                $this->form['captcha']="";
            });
        })->validate();
     

        $this->notify();
        $this->formSent();
      /*  dd('validated');
        $to = config('settings.contact_email','fabien@karsegard.ch');
        $mail = Mail::to($to);
        $cc = config('settings.contact_cc',NULL);
        if($cc){
            $mail->cc($cc);
        }
        $mail->send(new Notification($this->form));*/
        
    }
}
